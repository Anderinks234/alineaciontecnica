package com.bancolombia.microservice.alineacion.alineaciontecnica.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {

    @Value("${database.name}")
    private String databaseName;
    @Value("${mensaje.exito}")
    private String mensajeExito;

    public String getDatabaseName() {
        return databaseName;
    }

    public String getMensajeExito() {
        return mensajeExito;
    }
}
