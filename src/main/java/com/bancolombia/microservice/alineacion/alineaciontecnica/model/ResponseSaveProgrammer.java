package com.bancolombia.microservice.alineacion.alineaciontecnica.model;

public class ResponseSaveProgrammer  {

   private String message;
   private  String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseSaveProgrammer(String message, String code) {
        this.message = message;
        this.code = code;
    }
}
