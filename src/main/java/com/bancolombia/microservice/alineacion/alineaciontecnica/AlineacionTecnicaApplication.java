package com.bancolombia.microservice.alineacion.alineaciontecnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlineacionTecnicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlineacionTecnicaApplication.class, args);
	}

}
