package com.bancolombia.microservice.alineacion.alineaciontecnica.transaction;

import com.bancolombia.microservice.alineacion.alineaciontecnica.dao.AlineacionTecnicaDAO;
import com.bancolombia.microservice.alineacion.alineaciontecnica.model.ProgrammerRequest;
import com.bancolombia.microservice.alineacion.alineaciontecnica.model.ResponseSaveProgrammer;
import com.bancolombia.microservice.alineacion.alineaciontecnica.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AlineacionTecnicaTransaction {


    @Autowired
    AlineacionTecnicaDAO alineacionTecnicaDAO;

    @Autowired
    Constants constants;

    public ResponseEntity<ResponseSaveProgrammer>SaveProgrammer(ProgrammerRequest request) {
        ResponseEntity response = null;
        ResponseSaveProgrammer responseApi = null;
        try {
            alineacionTecnicaDAO.SaveProgrammer(request);
            responseApi = new ResponseSaveProgrammer(constants.getMensajeExito(), HttpStatus.OK.toString());
            response = new ResponseEntity<>(responseApi, null, HttpStatus.OK);
        } catch (Exception e) {
            responseApi = new ResponseSaveProgrammer(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.toString());
            response = new ResponseEntity<>(responseApi, null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;

    }


}
