package com.bancolombia.microservice.alineacion.alineaciontecnica.config;


import com.bancolombia.microservice.alineacion.alineaciontecnica.util.Constants;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Configuration {


    @Autowired
    private Constants constants;

    @Bean
    public Database programmerDatabase(CloudantClient cloudant) {

        return cloudant.database(constants.getDatabaseName(), true);
    }




}
