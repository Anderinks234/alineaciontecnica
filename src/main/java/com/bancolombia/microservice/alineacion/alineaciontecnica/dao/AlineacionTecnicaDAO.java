package com.bancolombia.microservice.alineacion.alineaciontecnica.dao;

import com.bancolombia.microservice.alineacion.alineaciontecnica.model.ProgrammerRequest;

import com.cloudant.client.api.Database;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AlineacionTecnicaDAO {

    @Autowired
    private Database programmerDatabase;

    public Object SaveProgrammer (ProgrammerRequest programmer)  {
        Object response = null;
        response =   programmerDatabase.save(programmer);
        return response;
    }
}
