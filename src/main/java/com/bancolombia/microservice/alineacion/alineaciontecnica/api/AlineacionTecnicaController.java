package com.bancolombia.microservice.alineacion.alineaciontecnica.api;

import com.bancolombia.microservice.alineacion.alineaciontecnica.model.ProgrammerRequest;
import com.bancolombia.microservice.alineacion.alineaciontecnica.model.ResponseSaveProgrammer;
import com.bancolombia.microservice.alineacion.alineaciontecnica.transaction.AlineacionTecnicaTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;

@Controller
public class AlineacionTecnicaController  implements iAlineacionTecnicaApi{


    @Autowired
    AlineacionTecnicaTransaction alineacionTecnicaTransaction;

    @Override
    public <T> ResponseEntity<T> getMessagePost(@Valid ProgrammerRequest body) {

        ResponseSaveProgrammer response = new ResponseSaveProgrammer( "Hola Mundo Spring boot!","200");

        //return (ResponseEntity<T>)  new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
        return  (ResponseEntity<T>) alineacionTecnicaTransaction.SaveProgrammer(body);


    }
}
