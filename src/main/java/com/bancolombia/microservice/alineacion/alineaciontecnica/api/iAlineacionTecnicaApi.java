package com.bancolombia.microservice.alineacion.alineaciontecnica.api;

import com.bancolombia.microservice.alineacion.alineaciontecnica.model.ProgrammerRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

public interface iAlineacionTecnicaApi {

    @RequestMapping (value = "${service.path}", consumes =  {"application/json"}, produces = {"application/json"}, method = RequestMethod.POST)
    <T> ResponseEntity <T> getMessagePost( @Valid @RequestBody ProgrammerRequest body);

}
